#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

#include "src/TrSolver.hpp"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

Eigen::IOFormat FullPrecisionFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");

//
TrSolverExitflag genericCase(VECTORVAR &gk, MATRIXVAR &Gk, double dk, int n, bool print) {
    VECTORVAR p = VECTORVAR::Zero(n);

    TrSolverOptions options = TrSolverOptions();
    options.maxIterations = 20;
    options.tolerance = 1e-8;

    TRSOL tr(n, true, true);
    tr.Solve(gk, Gk, dk, options, p);
    TrSolverInfo info = tr.getInfo();

    std::cout << "p: " << p.transpose().format(FullPrecisionFmt) << std::endl;
    std::cout << "lambda: " << info.lambda << std::endl;
    std::cout << "exitflag: " << info.exitflag << std::endl;
    std::cout << "running time: " << tr.getStatistics().cpuTime << " s" << std::endl;
    
    return info.exitflag;
}

TEST_CASE( "TrSolve test: Easy example 1", "[trsolve]" ) 
{
    // easy case
    double dk = 1;
    VECTORVAR gk = VECTORVAR::Zero(3);
    MATRIXVAR Gk = MATRIXVAR::Zero(3,3);
    gk << 5, 0, 4;
    Gk(0,0) = 1; Gk(0,1) = 0; Gk(0,2) = 4;
    Gk(1,0) = 0; Gk(1,1) = 2; Gk(1,2) = 0;
    Gk(2,0) = 4; Gk(2,1) = 0; Gk(2,2) = 3;
    Gk = ((Gk + Gk.transpose()) / 2.).eval();

    TrSolverExitflag res = genericCase(gk, Gk, dk, 3, true);
    REQUIRE(((res == TrSolverExitflag::ok) || (res == TrSolverExitflag::ok_hardcase)));
}
