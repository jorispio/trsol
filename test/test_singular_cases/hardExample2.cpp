#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

#include "src/TrSolver.hpp"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

Eigen::IOFormat FullPrecisionFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");

//
TrSolverExitflag genericCase(VECTORVAR &gk, MATRIXVAR &Gk, double dk, int n, bool print) {
    VECTORVAR p = VECTORVAR::Zero(n);

    TrSolverOptions options = TrSolverOptions();
    options.maxIterations = 50;
    options.tolerance = 1e-8;

    TRSOL tr(n, true, true);
    tr.Solve(gk, Gk, dk, options, p);
    TrSolverInfo info = tr.getInfo();

    std::cout << "p: " << p.transpose().format(FullPrecisionFmt) << std::endl;
    std::cout << "lambda: " << info.lambda << std::endl;
    std::cout << "exitflag: " << info.exitflag << std::endl;
    std::cout << "running time: " << tr.getStatistics().cpuTime << " s" << std::endl;
    return info.exitflag;
}

TrSolverExitflag problemCases(int icase)
{
    double dk = 2;
    VECTORVAR gk = VECTORVAR::Zero(3);
    MATRIXVAR Gk = MATRIXVAR::Zero(3,3);

    if (icase == 0) {
        gk << 1, 0, 4;
        Gk(0,0) = 0.69353341693984; Gk(0,1) = 1.1683061391294; Gk(0,2) = 0.40487760367793;
        Gk(1,1) =-0.297916230574926; Gk(1,2) = 0.187965282264424;
        Gk(2,2) = 0.131275354371315;
    }
    else if (icase == 1) {
        gk << -1.46662723520756, 0.160050420840295,  1.32168387747236;
        Gk << 0.793218912490915,  0.586441672663575, 0.0408812265472925,
            0.586441672663575,  0.441855144036178,  0.235789825769913,
            0.0408812265472925,  0.235789825769913, 0.0211159877459338;
    }
    else if (icase == 2) {
        gk << -1.16940005290638, 0.679832624070002, -1.39702307171451;
        Gk <<  0.37493075824631,   1.14688636738972,  -0.23036359867162,
                1.14688636738972, -0.151768454779519,  0.238775275052751,
                -0.23036359867162,  0.238775275052751,  0.458313899707434;
    }
    else if (icase == 3) {
        gk << -0.649068826440962, -0.125123624257634,  -1.04943660915474;
        Gk << -0.328227674598148,  0.535960571424187,  0.844514515478597,
                0.535960571424187,  0.825184683433987,  0.707279353767465,
                0.844514515478597,  0.707279353767465,  0.620359870702986;
    }
    else if (icase == 4) {
        gk << -1.57472033894083, -1.46634412639125, 0.608350850351954;
        Gk <<   0.901409678645142,    1.35311325172248,   -1.39076332808442,
            1.35311325172248,   0.573216582870815, -0.0303486807550026,
        -1.39076332808442, -0.0303486807550026,    1.52537373848404;
    }
    else if (icase == 5) {
        gk << -0.122419577782922,   1.28887604777223,  -1.82313246377066;
        Gk <<   1.42453519092451, -0.817454870561803,  0.467285389698085,
            -0.817454870561803,   0.31757873953794,  -1.66048030226401,
            0.467285389698085,  -1.66048030226401,   1.25788606953755;
    }

    Gk = ((Gk + Gk.transpose()) / 2.).eval();

    return genericCase(gk, Gk, dk, 3, true);
}

TEST_CASE( "TrSolve test: Hard example 2", "[trsolve]" ) 
{

    for (int icase = 0; icase < 5; ++icase) {
        TrSolverExitflag res = problemCases(icase);
        REQUIRE(((res == TrSolverExitflag::ok) || (res == TrSolverExitflag::ok_hardcase)));
    }
}
