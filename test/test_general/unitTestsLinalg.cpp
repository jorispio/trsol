#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <iostream>

#include "src/TrSolver.hpp"
#include "src/LinAlgUtils.hpp"

#include <catch2/catch.hpp>


TEST_CASE( "LinAlg test: upper triangular system solving", "[linalg]" ) {
    double tol = 1e-12;
    MATRIXVAR mA = MATRIXVAR::Zero(3,3);
    mA(0,0) = 1; mA(0,1) = 0; mA(0,2) = 4;
    mA(1,1) = 2; mA(1,2) = 0;
    mA(2,2) = 3;
    VECTORVAR b = VECTORVAR::Zero(3); b << -5, 0,-4;
    VECTORVAR x = VECTORVAR::Zero(3);
    solveTriangularSystem(mA, CholeskyTriangle::Upper, b, x);

    VECTORVAR bc = mA * x;
    REQUIRE(fabs(bc(0) - b(0)) < tol);
    REQUIRE(fabs(bc(1) - b(1)) < tol);
    REQUIRE(fabs(bc(2) - b(2)) < tol);
}

TEST_CASE( "LinAlg test: lower triangular system solving", "[linalg]" ) {
    double tol = 1e-12;
    MATRIXVAR mA = MATRIXVAR::Zero(3,3);
    mA(0,0) = 1;
    mA(1,0) = 0; mA(1,1) = 2;
    mA(2,0) = 4; mA(2,1) = 0; mA(2,2) = 3;
    VECTORVAR b = VECTORVAR::Zero(3); b << -5, 0,-4;
    VECTORVAR x = VECTORVAR::Zero(3);
    solveTriangularSystem(mA, CholeskyTriangle::Lower, b, x);

    VECTORVAR bc = mA * x;
    REQUIRE(fabs(bc(0) - b(0)) < tol);
    REQUIRE(fabs(bc(1) - b(1)) < tol);
    REQUIRE(fabs(bc(2) - b(2)) < tol);
}

TEST_CASE( "LinAlg test: generic linear system solving", "[linalg]" ) {
    double tol = 1e-12;
    MATRIXVAR mA = MATRIXVAR::Zero(3,3);
    mA(0,0) = 1;
    mA(1,0) = 0; mA(1,1) = 2;
    mA(2,0) = 4; mA(2,1) = 0; mA(2,2) = 3;
    VECTORVAR b = VECTORVAR::Zero(3);  b << -5, 0,-4;
    VECTORVAR x = VECTORVAR::Zero(3);
    solveLinearSystem(mA, b, x);

    VECTORVAR bc = mA * x;
    REQUIRE(fabs(bc(0) - b(0)) < tol);
    REQUIRE(fabs(bc(1) - b(1)) < tol);
    REQUIRE(fabs(bc(2) - b(2)) < tol);
}

TEST_CASE( "LinAlg test: compare system solving", "[linalg]" ) {
    double tol = 1e-12;
    MATRIXVAR R = MATRIXVAR::Zero(3,3);
    R(0,0) = 1; R(0,1) = 4; R(0,2) = 5;
    R(1,0) = 0; R(1,1) = 2; R(1,2) = 6;
    R(2,0) = 0; R(2,1) = 0; R(2,2) = 3;
    MATRIXVAR G = R.transpose() * R;
    VECTORVAR b = VECTORVAR::Zero(3);  b << -5, 0,-4;

    // solve: R' * R * x = b by substitution using triangular system, solving
    // R' * y = b, then R * x = y
    VECTORVAR x1 = VECTORVAR::Zero(3);
    VECTORVAR y = VECTORVAR::Zero(3);
    solveTriangularSystem(R.transpose(), CholeskyTriangle::Lower, b, y);
    solveTriangularSystem(R, CholeskyTriangle::Upper, y, x1);

    // solve: R' * R * x = b with inversion
   VECTORVAR x2 = VECTORVAR::Zero(3);
solveLinearSystem(G, b, x2);

    REQUIRE(fabs(x1(0) - x2(0)) < tol);
    REQUIRE(fabs(x1(1) - x2(1)) < tol);
    REQUIRE(fabs(x1(2) - x2(2)) < tol);
}

TEST_CASE( "LinAlg test: symmetric matrix detection", "[linalg]" ) {
    srand (time(NULL));

    VECTORVAR w = VECTORVAR::Zero(3);
    w(0) = rand() / (double)RAND_MAX;
    w(1) = rand() / (double)RAND_MAX;
    w(2) = rand() / (double)RAND_MAX;

    MATRIXVAR Q = MATRIXVAR::Identity(3,3) - 2 * w * w.transpose() / w.squaredNorm();

    MATRIXVAR D = MATRIXVAR::Zero(3, 3);
    D(0, 0) = 1;
    D(1, 1) = 2;
    D(2, 2) = 3;
    MATRIXVAR H = Q * D * Q.transpose();

    REQUIRE(isSymmetric(H));
}

TEST_CASE( "LinAlg performance test: matrix inversion vs backward substitution for triangular systems solving", "[linalg]" ) { // !benchmark
    int nruns = 100000;
    MATRIXVAR mA = MATRIXVAR::Zero(3,3);
    mA(0,0) = 1;
    mA(1,0) = 0; mA(1,1) = 2;
    mA(2,0) = 4; mA(2,1) = 0; mA(2,2) = 3;
    VECTORVAR b = VECTORVAR::Zero(3);
    VECTORVAR x = VECTORVAR::Zero(3);

    unsigned int start_time = clock();
    BENCHMARK("Matrix inversion") {
        for (int irun=0; irun<nruns; ++irun) {
            solveLinearSystem(mA, b, x);
        }
    }
    unsigned int stop_time = clock();
    double cputimeGeneric = difftime(stop_time, start_time);

    start_time = clock();
    BENCHMARK("Backward substitution") {
        for (int irun=0; irun<nruns; ++irun) {
            solveTriangularSystem(mA, CholeskyTriangle::Lower, b, x);
        }
    }
    stop_time = clock();
    double cputimeTriangular = difftime(stop_time, start_time);

    //printf("\nMatrix inversion system solving    %8.3f s", cputimeGeneric);
    //printf("\nSubstitution method system solving %8.3f s", cputimeTriangular);
    REQUIRE(cputimeTriangular < cputimeGeneric);
}

