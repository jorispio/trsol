#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

#include "src/TrSolver.hpp"
#include "src/TrSolverOptions.hpp"
#include "src/TrSolverCholeskyDecomposition.hpp"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#define NRUN 100000

Eigen::IOFormat FullFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");

//
void genericCase(VECTORVAR &gk, MATRIXVAR &Gk, double dk, int n) {
	VECTORVAR p = VECTORVAR::Zero(n);

	TrSolverOptions options = TrSolverOptions();	
	options.maxIterations = 100;
    options.tolerance = 1e-8;	

	TRSOL tr(n);
	double lambda = tr.Solve(gk, Gk, dk, options, p);   
}

TEST_CASE( "Benchmark: simple trsolve", "[benchmark]" ) {
    double delta = 2;
	VECTORVAR gk = VECTORVAR::Zero(3);
	MATRIXVAR Gk = MATRIXVAR::Zero(3,3);	
	
	gk << 5, 0, 4;
	Gk << 1, 0, 4,
            0,2,0,
            4,0,3;
	
    unsigned int start_time = clock();
    BENCHMARK("TR solving") {
        for(int i=0; i<NRUN; i++) {        
            genericCase(gk, Gk, delta, 3);
        }
    }
    unsigned int stop_time = clock();
    double cputime = difftime(stop_time, start_time);
	printf("\nBenchmark: %d runs. cputime = %f", NRUN, cputime / double(CLOCKS_PER_SEC));	
}
