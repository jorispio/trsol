#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <iostream>

#include <Eigen/Cholesky>
#include <Eigen/Eigenvalues>

#include "src/TrSolver.hpp"
#include "src/TrSolverData.hpp"
#include "src/TrSolverCholeskyDecomposition.hpp"

#include <catch2/catch.hpp>


Eigen::IOFormat FullFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");


void testCholeskyPositive(const MATRIXVAR &Gk, int n) {
    double tol = 1e-13;

    MATRIXVAR mU = MATRIXVAR::Zero(n,n);
    int cholOut1 = CholeskyDecomposition::matrixcholesky(Gk, n, CholeskyTriangle::Upper, mU);

    Eigen::LLT<MATRIXVAR, Eigen::Upper> rrtOfA(Gk); // compute the Cholesky decomposition
    MATRIXVAR mUref = rrtOfA.matrixU();
    // compare
    REQUIRE(rrtOfA.info() == Eigen::ComputationInfo::Success);
    REQUIRE(cholOut1 == 0);
    REQUIRE((mU - mUref).norm() < tol);

    MATRIXVAR mL = MATRIXVAR::Zero(n,n);
    int cholOut2 = CholeskyDecomposition::matrixcholesky(Gk, n, CholeskyTriangle::Lower, mL);

    Eigen::LLT<MATRIXVAR, Eigen::Lower> lltOfA(Gk); // compute the Cholesky decomposition
    MATRIXVAR mLref = lltOfA.matrixL();
    // compare
    REQUIRE(lltOfA.info() == Eigen::ComputationInfo::Success);
    REQUIRE(cholOut2 == 0);
    REQUIRE((mL - mLref).norm() < tol);
}

void testCholeskyNonPositive(MATRIXVAR &Gk, int n) {
    MATRIXVAR mU = MATRIXVAR::Zero(n,n);
    int cholOut1 = CholeskyDecomposition::matrixcholesky(Gk, n, CholeskyTriangle::Upper, mU);

    Eigen::LLT<MATRIXVAR, Eigen::Upper> rrtOfA(Gk); // compute the Cholesky decomposition
    MATRIXVAR mUref = rrtOfA.matrixU();
    // compare
    REQUIRE(!(cholOut1 == 0));
    REQUIRE(rrtOfA.info() != Eigen::ComputationInfo::Success);

    MATRIXVAR mL = MATRIXVAR::Zero(n,n);
    int cholOut2 = CholeskyDecomposition::matrixcholesky(Gk, n, CholeskyTriangle::Lower, mL);

    Eigen::LLT<MATRIXVAR, Eigen::Lower> lltOfA(Gk); // compute the Cholesky decomposition
    MATRIXVAR mLref = lltOfA.matrixL();
    // compare
    REQUIRE(!(cholOut2 == 0));
    REQUIRE(lltOfA.info() != Eigen::ComputationInfo::Success);
}

TEST_CASE( "Cholesky test: symmetric positive definite", "[cholesky]" ) {
    MATRIXVAR Gk = MATRIXVAR::Zero(3,3);
    Gk << 1, 0, 0,
            0, 2, 0,
            0, 0, 3;
    testCholeskyPositive(Gk, 3);

    Gk << 1, 0, 4,
            0, 2, 0,
            4, 0, 19;
    testCholeskyPositive(Gk, 3);

    MATRIXVAR R = MATRIXVAR::Zero(3,3);
    R << 1, 0, 4,
            0, 2, 0,
            4, 0, 3;
    testCholeskyPositive(R.transpose() * R, 3);
}

TEST_CASE( "Cholesky test: symmetric non-positive definite", "[cholesky]" ) {
    MATRIXVAR Gk = MATRIXVAR::Zero(3,3);
    Gk << 1, 0, 0,
            0, 2, 0,
            0, 0, -1;
    testCholeskyNonPositive(Gk, 3);

    Gk = MATRIXVAR::Zero(3,3);
    Gk << 1, 0, 4,
            0, 2, 0,
            4, 0, 3;
    testCholeskyNonPositive(Gk, 3);
}

TEST_CASE( "Cholesky test: with smallest eigen value", "[cholesky]" ) {
    MATRIXVAR Gk = MATRIXVAR::Zero(3,3);
    Gk <<   1.42453519092451, -0.817454870561803,  0.467285389698085,
        -0.817454870561803,   0.31757873953794,  -1.66048030226401,
        0.467285389698085,  -1.66048030226401,   1.25788606953755;
    testCholeskyNonPositive(Gk, 3);

    Eigen::VectorXcd eivals = Gk.eigenvalues();
    real_type lambda = -eivals.real().minCoeff() * (1 + 1000 * epsil);
    testCholeskyPositive(Gk + lambda * MATRIXVAR::Identity(3,3), 3);
}
