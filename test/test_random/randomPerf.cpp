#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <random>

#include "src/TrSolver.hpp"
#include "src/TrSolverOptions.hpp"

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#define NRUN 100

// -------------------------------------------------------------------------
Eigen::IOFormat FullFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");

std::random_device rd{};
std::default_random_engine generator{rd()};
std::normal_distribution<double> distribution(0.0, 1.0);

// -------------------------------------------------------------------------
void createProblem(bool useRandom, const MATRIXVAR &D, const VECTORVAR &g, VECTORVAR &gk, MATRIXVAR &Gk)
{
    if (useRandom) {
        for (int irow = 0; irow < 3; ++irow) {
            gk(irow) = distribution(generator);

            for (int jcol = 0; jcol <= irow; ++jcol) {
                Gk(irow,jcol) = (1 + distribution(generator)) / 2.;
            }
        }
        Gk = ((Gk + Gk.transpose()) / 2.).eval();
    } else {
        VECTORVAR w = VECTORVAR::Zero(3);
        w(0) = (1 + distribution(generator)) / 2.;
        w(1) = (1 + distribution(generator)) / 2.;
        w(2) = (1 + distribution(generator)) / 2.;

        MATRIXVAR Q = MATRIXVAR::Identity(3,3) - 2 * w * w.transpose() / w.squaredNorm();
        Gk = Q * D * Q.transpose();
        gk = Q * g;
    }
}
// -------------------------------------------------------------------------
//
TrSolverInfo genericCase(const VECTORVAR &gk, const MATRIXVAR &Gk, double dk, int n)
{
    VECTORVAR p = VECTORVAR::Zero(n);

    TrSolverOptions options = TrSolverOptions();
    options.maxIterations = 1000;
    options.tolerance = 1e-8;

    TRSOL tr(n);
    tr.Solve(gk, Gk, dk, options, p);
    return tr.getInfo();
}

// -------------------------------------------------------------------------
double benchmark(std::string title, std::string filename, bool useRandom, const MATRIXVAR &D, const VECTORVAR &g)
{
    double delta = 2;
    VECTORVAR gk = VECTORVAR::Zero(3);
    MATRIXVAR Gk = MATRIXVAR::Zero(3,3);

    std::ofstream file(filename);

    // Benchmark
    int success[] = {0, 0, 0, 0};
    int nbValidProblem = 0;
    unsigned int start_time = clock();
    for(int i=0; i<NRUN; i++) {

        createProblem(useRandom, D, g, gk, Gk);

        TrSolverInfo info = genericCase(gk, Gk, delta, 3);
        TrSolverExitflag exitflag = info.exitflag;
        if (exitflag >= 0) { //TrSolverExitflag::ok || exitflag == TrSolverExitflag::ok_hardcase) {
            ++success[0];
        }
        else {
            // store case to file
            if (file.is_open()) {
                file << "gk:\n" << gk.transpose().format(FullFmt) << '\n';
                file << "Gk:\n" << Gk.format(FullFmt) << '\n';
                file << "p: " << info.p.transpose().format(FullFmt) << '\n';
                file << "||p|| " << info.p.norm() << " (delta=" << delta  << ") error=" << info.p.norm() - delta  << " iterations: " << info.iterations << "\n";
                file << "lambda:" << info.lambda << "\n";
                file << "exitflag: " << info.exitflag << "\n";
                file << "---------------------\n";
            }

            if (exitflag == TrSolverExitflag::too_many_iterations) {
                ++success[1];
            }
            else if (exitflag == rounding_error) {
                ++success[2];
            }
            else {
                ++success[3];
            }
        }

        if (exitflag > -98) {
            ++nbValidProblem;
        }
    }

    file.close();

    unsigned int stop_time = clock();
    double cputime = difftime(stop_time, start_time);
    printf("\n*** %s ***\n", title.c_str());
    printf("Benchmark: %d runs. cpu time = %f s\n", NRUN, cputime / double(CLOCKS_PER_SEC));
    printf("           %.0f %% success  (%.0f valid problems)\n", (100. * success[0]) / double(nbValidProblem), (100. * nbValidProblem) / double(NRUN));
    printf("              %.0f %% too_many_iterations\n", (100. * success[1]) / double(NRUN));
    printf("              %.0f %% rounding_error\n", (100. * success[2]) / double(NRUN));
    printf("              %.0f %% other issue\n", (100. * success[3]) / double(NRUN));
    
    return (100. * success[0]) / double(nbValidProblem);
}

// -------------------------------------------------------------------------
TEST_CASE( "TrSolve benchmark: positive definite problems", "[trsolve]" )
{
    MATRIXVAR D = MATRIXVAR::Zero(3, 3);
    D(0, 0) = 1;
    D(1, 1) = 2;
    D(2, 2) = 3;
    VECTORVAR g = VECTORVAR::Zero(3);
    g << 1, 0, 2;
    double res = benchmark("Positive Definite Problems", "ProblematicCases.txt", false, D, g);
    REQUIRE(res > 99.9);
}

TEST_CASE( "TrSolve benchmark: saddle point problems", "[trsolve]" ) 
{
    MATRIXVAR D = MATRIXVAR::Zero(3, 3);
    D(0, 0) = 1;
    D(1, 1) = 2;
    D(2, 2) = 3;
    VECTORVAR g = VECTORVAR::Zero(3);
    g << 0, 0, 0;
    double res = benchmark("Saddle point Problems", "ProblematicCases-saddle.txt", false, D, g);
    REQUIRE(res > 99.9);
}

TEST_CASE( "TrSolve benchmark: semi-definite problems", "[trsolve]" )
{    
    MATRIXVAR D = MATRIXVAR::Zero(3, 3);
    VECTORVAR g = VECTORVAR::Zero(3);
    D(0, 0) = 1;
    D(1, 1) = -1;
    D(2, 2) = 3;
    g << 1, 0, 2;
    double res = benchmark("Semi-Positive Definite Problems", "ProblematicCases-semipos.txt", false, D, g);
    REQUIRE(res > 99.9);
}

TEST_CASE( "TrSolve test: random problems", "[trsolve]" )
{    
    MATRIXVAR D = MATRIXVAR::Zero(3, 3);
    VECTORVAR g = VECTORVAR::Zero(3);
    double res = benchmark("Random Problems", "ProblematicCases-random.txt", true, D, g);
    REQUIRE(res > 99.9);
}
