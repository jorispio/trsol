README
=========

## Preamble

This is a trust-region solver for small to medium-size dense problems.

Trust-region problem:
Consider a smooth function f(x).
It can be approximated quadratically in the neighborhood of a point x0:
```
s' g + 1/2 s' H s
```
where ```s = x-x0```.

This approximation is only valid on an Euclidean ball, B(x0, r), of radius r > 0.
The trust-region solver solved the problem of minimization of f in B, with the validity constraint x in B.

If H is positive-definite, the problem is solved in one step.
It happens though (and this is the fun part!) that the problem has no extrema in B (H is only semi-definite positive).
The solution is then on the boundary, ```||x-x0|| = r```, and in addition, the solver returns the Lagrange variable lambda so that ```H + lambda I > 0```.

The resolution follows the Moré-Sorensen method, where the problem is solved using Newton step iterations with several matrix factorization. The resolution speed is quadratic.
Other methods exist and show faster convergence rates for hard cases (they are not implemented here for now...).


### Examples
Examples (Branin function, Rosenborck function, Mishra's bird, Woods) are provided to illustrate the usage of the solver library.

<img src="examples/ex2_branin/branin_colormap.png" width=250/>
<img src="examples/ex3_rosenbrock/rosenbrock_colormap.png" width=250/>

### Side note
The implementation uses efficient math libraries (Eigen).

However, this solver implementation has not the ambition to beat current Trust-region solvers in terms of accuracy or speed. It actually mainly aims at being used as an underlying block for a higher level solver for small dense problems, even though I included implementation and examples showing it can solve efficiently nonlinear problems.
It represents the current implementation I use in an other nonlinear optimizer where I seek to reduce execution speed.


## Compiling and Running

### Requirements
1. A C++11 compiler (The build chain is based on CMake)
2. The library should compile on Windows and Linux. I have actually tested it on Windows (Cygwin) and Linux.
3. The library use Eigen.

### Download the sources
Download the latest version source code from the main page as a zip.
Download the latest version of [Eigen] library and unzip in the thirdparty folder.

[Eigen]: http://eigen.tuxfamily.org/

### Compiling
The building and compilation is based on cross platform CMake configuration file.
Go into the root folder.
In a console, type
```
cmake -DCMAKE_INSTALL_PREFIX=[your install path] .
```

where [your install path] defines the installation destination directory ("prefix").
(If omitted, the build would most probably be installed in /usr/local.)

Then:
```
make
make install
```

The build [your install path] folder should contain:
- an executable,
- lib directory with the static and share libraries,
- the Python package,
- examples directory.

If you want to compile the unit tests, add DBUILD_TESTS to the cmake call,
```
cmake -DBUILD_TESTS=ON .
```

### Running
Compile your application with the just built static library.


## References
1. J.J. Moré & D.C. Sorensen, "*Computing A Trust Region Step*," SIAM J. Sci. Stat. Comp. **4**, 553-572 (1983).
