PROJECT(trsol)
cmake_minimum_required(VERSION 3.3)

OPTION(BUILD_TESTS "Build test programs" ON)
OPTION(BUILD_EXAMPLES "Build example programs" ON)
OPTION(USE_EIGEN_CHOLESKY "Use EIGEN Cholesky decomposition" ON)

message(STATUS "Installation prefix: ${CMAKE_INSTALL_PREFIX}")
message(STATUS "Use EIGEN Cholesky decomposition: ${USE_EIGEN_CHOLESKY}")
message(STATUS "Build tests: ${BUILD_TESTS}")

# ===============================================================

ADD_SUBDIRECTORY(thirdparty)
ADD_SUBDIRECTORY(src)

IF (BUILD_TESTS)
    ADD_SUBDIRECTORY(test)
ENDIF()

IF (BUILD_EXAMPLES)
    ADD_SUBDIRECTORY(examples)
ENDIF()

