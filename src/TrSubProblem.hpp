/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef TR_SUB_PROBLEM_HPP
#define TR_SUB_PROBLEM_HPP

#include "TrSolver.hpp"
#include "TrSolverOptions.hpp"

// -------------------------------------------------------------------------
class TrSubProblem {
private:
	TRSOL *tr;
	int ndim;
	TrSolverOptions options;
	double lambda;	
	
	VECTORVAR gk;
    MATRIXVAR Gk;

public:
	TrSubProblem(int n);
	TrSubProblem(int n, const TrSolverOptions &options);
	virtual ~TrSubProblem();

	void setOptions(const TrSolverOptions &optionsIn);
	int getProblemSize() { return ndim; }
	
	virtual double fun(const VECTORVAR &x) = 0;
	virtual void gradient(const VECTORVAR &x, VECTORVAR &gk) = 0;
	virtual void hessian(const VECTORVAR &x, MATRIXVAR &Gk) = 0;
	
	double solve(const VECTORVAR &x, double dk, VECTORVAR &p);
	
	double getLagrangeValue();
	MATRIXVAR getPositiveHessian();
	VECTORVAR getCurrentGradient() const { return gk; }
	TRSOL *getSolver() const { return tr; }
};

// -------------------------------------------------------------------------
#endif