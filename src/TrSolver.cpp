/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * Source:
 *    Moré, J.J. & Sorensen, D.C., Computing A Trust Region Step", Argonne National Laboratory, Dec. 1981
 *
 */
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>       // CLOCKS_PER_SEC

#include <Eigen/Eigenvalues>

#include "TrSolver.hpp"
#include "TrSolverData.hpp"
#include "TrSolverCholeskyDecomposition.hpp"
#include "LinAlgUtils.hpp"


//#define USE_CLINE_CONN_VANLOAN // to use Cline - Conn - Van Loan algorithm

// use eigen value to estimate singular value of Lagrange multiplier when not positive-definite
// this is of course much more expensive, but more robust.
#define USE_EIGEN_VALUE_WHEN_NPD

//#define DEBUG

// -------------------------------------------------------------------------
// MACROS
#ifdef DEBUG
Eigen::IOFormat TrSolverFullPrecFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");
#endif

#define max(a,b)  ((a>b)?a:b)
#define min(a,b)  ((a<b)?a:b)
#define sign(a)  ((a>0)?1.:-1.)

// -------------------------------------------------------------------------
TRSOL::TRSOL(uint n, bool pos_def, bool debug)
   :ndim(n),
    positive_definite(pos_def),
	iterations(0),
	debug(debug)
{
	info.n = n;
	info.iterations = 0;
	info.lambda = 0;
	info.gradient = VECTORVAR::Zero(n);
	info.hessian = MATRIXVAR::Zero(n, n);
	info.p = VECTORVAR::Zero(n);
	info.exitflag = TrSolverExitflag::ok;

	resetStatistics();
}
// -------------------------------------------------------------------------
void TRSOL::resetStatistics() {
	statistics.nbCholesky = 0;
	statistics.cpuTime = 0;
}
// -------------------------------------------------------------------------
bool TRSOL::checkOptions() {
    if ((options.tolerance < 0) || (options.tolerance > 1)
            || (options.abstolerance < 0) || (options.abstolerance > 1)
            || (options.maxIterations < 1)) {
        info.exitflag = TrSolverExitflag::invalid_input;
        return false;
    }
    return true;
}
// -------------------------------------------------------------------------
bool TRSOL::checkInputs(const MATRIXVAR &Gk, const VECTORVAR &gk) {
    if (Gk.rows() != Gk.cols()) {
        info.exitflag = TrSolverExitflag::hessian_is_not_square;
        return false;
    }
    if (gk.rows() != Gk.rows()) {
        info.exitflag = TrSolverExitflag::invalid_dimensions;
        return false;
    }
    if (!isSymmetric(Gk)) {
        info.exitflag = TrSolverExitflag::hessian_is_not_symmetric;
        return false;
    }
    return true;
}
// -------------------------------------------------------------------------
/** Solve the trust-region problem:
 *       Min Fx * d + 1/2 * d' * Fxx * d
 *           s.c.  ||d|| < delta
 *  delta>0.
 *
 * Inputs:
 *       Gk      Hessian, symmetric matrix
 *       gk      gradient
 *    delta      trusted region
 *
 * return the solution p and,
 * lambda, the Lagrange multiplier, lambda >= 0, for the constraint: psi(d) = ||d|| - delta
 * making: Gk + lambda I >= 0.
 */
// -------------------------------------------------------------------------
real_type TRSOL::Solve(const VECTORVAR &gk, const MATRIXVAR &Gk, real_type delta,
        const TrSolverOptions &options,
        VECTORVAR &pSolution)
{
    resetStatistics();

    if (!checkOptions()) {
        return 0;
    }
    if (!checkInputs(Gk, gk)) {
        return 0;
    }
    if (delta <= 0) {
        info.exitflag = TrSolverExitflag::invalid_input;
        return 0;
    }

    #ifdef DEBUG
        printf("tol = %g\n", (double)options.tolerance);
        printf("maxiter = %d\n", options.maxIterations);
        printf("delta = %f\n", (double)delta);
        std::cout << "g =  " << gk.transpose() << std::endl;
        std::cout << "G =  " << Gk << std::endl;
    #endif

    if(positive_definite) {
        return pSolve(gk, Gk, delta, ndim, options, pSolution);
    }
    else {
        // negative definite
        return -pSolve(-gk, -Gk, delta, ndim, options, pSolution);
    }
}
// -------------------------------------------------------------------------
/** Solve the trust-region problem with elliptic constraint on the radius:
 *       Min Fx * d + 1/2 * d' * Fxx * d
 *           s.c.  ||d|| < delta
 *  delta in R^n+
 *
 * Inputs:
 *       Gk      Hessian Fxx, symmetric matrix
 *       gk      gradient Fx
 *    delta      trusted region
 *
 * return the solution p and,
 * lambda, the Lagrange multiplier making Gk + lambda I > 0.
 */
// -------------------------------------------------------------------------
real_type TRSOL::Solve_Ellipt(const VECTORVAR &gk, const MATRIXVAR &Gk, const VECTORVAR &delta,
        const TrSolverOptions &options,
        VECTORVAR &pSol)
{
    real_type tr = delta.maxCoeff();
    MATRIXVAR iP = delta.asDiagonal();
    iP /= tr;

    MATRIXVAR H = iP * Gk * iP;
    VECTORVAR G = iP * gk;
    return Solve(G, H, tr, options, pSol);
}
// -------------------------------------------------------------------------
real_type TRSOL::initialiseLagrange(const MATRIXVAR &Gk, const VECTORVAR &gk, real_type delta,
            real_type &lmbLower, real_type &lmbUpper, real_type &lmbSingular) {
    double normMatGk = normMatrix(Gk, ndim, ndim);
    lmbSingular = (-Gk.diagonal()).maxCoeff();	// max negative diagonal element of G
    lmbLower = max( epsil, max( lmbSingular, gk.norm()/delta - normMatGk )); // a lower bound
    lmbUpper = max( epsil, gk.norm()/delta + normMatGk); // an upper bound

    if (fabs(lmbLower - lmbUpper) < epsil) {
        lmbLower *= (1 - options.tolerance);
        lmbUpper *= (1 + options.tolerance);
    }

    real_type lambda = sqrt(lmbUpper * lmbLower);

    if (debug) {
        printf("\tlmbS = %f, lmbL = %f, lmbU = %f,||gk|| = %f, ||Gk|| = %f\n", lmbSingular, lmbLower, lmbUpper, gk.norm() / delta, normMatGk);
    }

    return lambda;
}
// -------------------------------------------------------------------------
/**
 * Trust Region
 * The function is quadratically approximated. We look for the maximum delta
 * which ensure the approximation to still be correct.
 *
 * Solve:
 *       Min Fx*d + 1/2*d'*Fxx*d
 *           s.c.  D*x < delta
 *
 * Where D>0 and delta>0 are given.
 *
 * Futhermore, if x* is a global minimizer of the quadratic approximation,
 *    (Fxx + lambda I) x* = -c
 * with lambda >= 0, (Fxx + lambda I) >= 0
 *
 * Indeed, the hard case is the one where we there is no solution for D*x = delta
 * for which (Fxx + lambda I) >= 0. In this case, we need to use the Eigen vector
 * space.
 *
 * Inputs:
 *       Gk      Hessian  (nxn)
 *       gk      gradient (nx1)
 *    delta      trusted region
 *       n       TR subproblem size
 *  options      TR subproblem solver option
 *
 * Outputs:
 *    pSol       Solution of the TR subproblem
 *  return lambda, the Lagrange multiplier for the constraint psi(d) = ||d|| - delta
 *
 */
// -------------------------------------------------------------------------
real_type TRSOL::pSolve(const VECTORVAR &gk, const MATRIXVAR &Gk, real_type dk, int n, TrSolverOptions options,
        VECTORVAR &pSol)
{
    real_type lambda = 0;
    unsigned int start_time = clock();

    // reset p for restart
    p = VECTORVAR::Zero(ndim);
    MATRIXVAR R = MATRIXVAR::Zero(ndim, ndim);

    // simple case, test whether the minimizer is within the trust region
    int chlskyInfo = CholeskyDecomposition::matrixcholesky(Gk, n, CholeskyTriangle::Upper, R);
    if (chlskyInfo == 0) { // positive definite
        if (SolveSimple(R, gk, dk, pSol, lambda)) {
            info.iterations = 0;
            info.lambda = lambda;
            info.gradient = gk;
            info.hessian = Gk;
            info.p = pSol;
            info.exitflag = TrSolverExitflag::ok;
            return lambda;
        }
    }

    /*
     * Now we know that the solution is on the boundary of the domain.
     * We try to solve the non-linear secular equation:
     *
     *  || p(lambda) ||^beta = delta^beta
     *
     * See Moré and Sorensen (beta = -1), solving:
     *  1 / || p(lambda) || =  1 / delta
     * to first order, which proves to be solved with quadratic convergence.
     * We apply a Newton's method, and determine lambda.
     *
     */
    double lmbLower, lmbUpper, lmbSingular;
    lambda = initialiseLagrange(Gk, gk, dk, lmbLower, lmbUpper, lmbSingular);

    MATRIXVAR identity = MATRIXVAR::Identity(ndim, ndim);
    VECTORVAR zhat = VECTORVAR::Zero(ndim);

    #ifdef DEBUG
        printf("Begin loop (dk = %f, lambda = %f, ||p|| = %f)\n", dk, lambda, p.norm());
    #endif
    int maxMinorIterations = 10;
    iterations = 0;
    int iterMinor = 0;
    info.exitflag = TrSolverExitflag::too_many_iterations;
    bool done = false;
    while ( !done && (iterations < options.maxIterations) && (iterMinor < maxMinorIterations)) {

        // safeguard lambda so that it is always in [lmbLower, lmbUpper]
        lambda = safeguardLagrange(lmbSingular, lmbLower, lmbUpper, lambda);

        // factor: G + lambda I = R' R
        R = MATRIXVAR::Zero(ndim, ndim);
        MATRIXVAR B = Gk + lambda * identity;
        chlskyInfo = CholeskyDecomposition::matrixcholesky(B, n, CholeskyTriangle::Upper, R);
        ++statistics.nbCholesky;
        if (chlskyInfo == 0) {
            iterMinor = 0;

            // solve R' R p = -g
            VECTORVAR x = VECTORVAR::Zero(ndim);
            solveTriangularSystem(R.transpose(), CholeskyTriangle::Lower, -gk, x);
            solveTriangularSystem(R, CholeskyTriangle::Upper, x, p);

            if (debug) {
                printf("\tNewton step: ||p||=%g\n", p.norm());
            }

            bool useNegativeCurvature = false;
            real_type tau = 0;
            if (fabs(p.norm() - dk) < options.tolerance) {
                info.exitflag = TrSolverExitflag::ok;
            }
            else if ((p.norm() < dk) /*&& (lambda >= 0)*/) {
                // hard case, take the direction of negative curvature
                // using an approximate direction of the eigen vector corresponding to lambda_1 (smallest eigen value)
                useNegativeCurvature = true;
                tau = getZhatAndTau(R, p, dk, zhat);

                VECTORVAR Rzhat = R * zhat;
                lmbSingular = max(lmbSingular, lambda - Rzhat.squaredNorm());

                real_type RpSquared = (R * p).squaredNorm();
                if (debug) {
                    printf("\tlmbSingular = %f\n", lmbSingular);
                    printf("\tstop. cond: %g <? %g\n", tau * tau * (R * zhat).squaredNorm(), options.tolerance * (2 - options.tolerance) * max(options.abstolerance, RpSquared + lambda * dk * dk));
                }

                // break if one of the stopping conditions satisfied
                if (tau * tau * (R * zhat).squaredNorm() < options.tolerance * (2 - options.tolerance) * max(options.abstolerance, RpSquared + lambda * dk * dk)) {
                    if (debug) {
                        printf("\taccepting negative curvature improvement\n");
                    }

                    info.exitflag = TrSolverExitflag::ok_hardcase;
                    done = true;
                }
                else if (RpSquared + lambda*dk*dk < 2 * options.abstolerance) {
                    info.exitflag = TrSolverExitflag::ok_abs_accuracy;
                    break;
                }
                else if (p.norm() < epsil) {
                    info.exitflag = TrSolverExitflag::ok_hardcase;
                    break;
                }
            }

            real_type pnorm = p.norm();
            if (pnorm > dk) lmbLower = max(lmbLower, lambda);
            if (pnorm < dk) lmbUpper = min(lmbUpper, lambda);

            if (gk.norm() > 0) {
                lambda = updateLagrangeMultiplier(R, p, dk, lambda);
                lambda = max(lambda, lmbLower);
            }

            if (useNegativeCurvature) {
                p = p + tau * zhat;
            }

            ++iterations;
        }

        if (!(chlskyInfo == 0)) {
            // degenerate case, try to improve using rank information
            // note that R is an intermediate matrix which sub-matrix of rank i is upper triangular.
            lmbSingular = estimateSingularValue(B, R, lambda, lmbSingular, chlskyInfo);

            // to prevent case where lambdaUpper is the optimal value of lambda
            lmbUpper = max(lmbUpper, (1 + options.tolerance) * lmbSingular);

            // 3.14.7
            lambda = (1 + 0.5 * options.tolerance) * lmbSingular;

            ++iterMinor;
        }

        // update lower bound using singular value
        lmbLower = max(lmbLower, (1 - options.tolerance) * lmbSingular);

        #ifdef DEBUG
            printf("major=%2d, minor=%2d ||p|| = %f, dk = %f, l_l = %f, l = %f, l_u = %f (ls = %f) isPd=%d\n", iterations, iterMinor, p.norm(), dk, lmbLower, lambda, lmbUpper, lmbSingular, chlskyInfo==0);
        #endif
        if (debug) {
            printf("    err(p) = %g  (tol=%g)\n", fabs(dk - p.norm()), options.tolerance * max(1, dk));
            printf("major=%2d, minor=%2d ||p|| = %f, dk = %f, l_l = %f, l = %f, l_u = %f (ls = %f) isPd=%d\n\n", iterations, iterMinor, p.norm(), dk, lmbLower, lambda, lmbUpper, lmbSingular, chlskyInfo==0);
        }

        // stopping criteria
        if (chlskyInfo == 0) {
            if (fabs(dk - p.norm()) < options.tolerance * max(1, dk)) {
                info.exitflag = TrSolverExitflag::ok;
                done = true;
            }
            else if ( (p.norm() <= dk) && (fabs(lambda) < epsil) ) {
                // we are within the trust region !
                info.exitflag = TrSolverExitflag::ok;
                done = true;
            } else if (lmbUpper <= (1 + 0.5 * options.tolerance) * lmbSingular) {
                info.exitflag = TrSolverExitflag::rounding_error;
                done = true;
            }
        }
    }

    #ifdef DEBUG
        printf("End loop (iter = %d)\n", iterations);
    #endif
    pSol = p;
    info.iterations = iterations;
    info.lambda = lambda;
    info.gradient = gk;
    info.hessian = Gk;
    info.p = p;

    unsigned int stop_time = clock();
    statistics.cpuTime = difftime(stop_time, start_time) / CLOCKS_PER_SEC;

    return lambda;
}

// -------------------------------------------------------------------------
bool TRSOL::SolveSimple(const MATRIXVAR &R, const VECTORVAR &gk, double dk, VECTORVAR &pSol, real_type &lambda) {
    // better use R triangular than Gk
    VECTORVAR x = VECTORVAR::Zero(ndim);
    solveTriangularSystem(R.transpose(), CholeskyTriangle::Lower, -gk, x);
    solveTriangularSystem(R, CholeskyTriangle::Upper, x, p);

    #ifdef DEBUG
        printf("||p|| = %f, dk = %f\n", p.norm(), dk);
    #endif

    if (p.norm() < dk) {
        pSol = p;
        lambda = 0;
        return true;
    }
	return false;
}

// -------------------------------------------------------------------------
/** Updates lambda when H=R'*R is positive definite.
 * q must be of size n
 */
// -------------------------------------------------------------------------
real_type TRSOL::updateLagrangeMultiplier(MATRIXVAR &R, VECTORVAR &p, real_type delta, real_type lambda)
{
    // solve R' q = p
    VECTORVAR q = VECTORVAR::Zero(ndim);
    solveTriangularSystem(R.transpose(), CholeskyTriangle::Lower, p, q);

    real_type nq2 = q.squaredNorm();
    real_type np2 = p.squaredNorm();
    real_type newlambda = lambda;
    if (nq2 > 0) {
        newlambda = lambda + (np2 / nq2) * (sqrt(np2) - delta) / delta;
    }
    if (debug) {
        printf("\tUpdate Lagrange Multiplier: ||p||=%g  ||q||=%g  lambda: %g -> %g\n", p.norm(), q.norm(), lambda, newlambda);
    }
    return newlambda;
}
// -------------------------------------------------------------------------
/** Safeguarding function for lambda
 * This function ensures that lambda is in [lambdaLower, lambdaUpper], and
 * far enough from singular value.
 */
// -------------------------------------------------------------------------
real_type TRSOL::safeguardLagrange(real_type lmbSingular, real_type lmbLower, real_type lmbUpper, real_type lambda)
{
    real_type newlambda = min(max(lambda, lmbLower), lmbUpper);
    if (lambda < lmbSingular) {
        newlambda = max(0.001 * lmbUpper, sqrt(lmbLower * lmbUpper));
    }
    if (debug) {
        printf("\tSafeguard: [%f %f] lmbSingular=%f lambda=%f  newlambda=%f\n", lmbLower, lmbUpper, lmbSingular, lambda, newlambda);
    }
    return newlambda;
}

// -------------------------------------------------------------------------
/** Compute the direction of negative curvature.
 * With ||u||=1 and ||p||<1, there are two choices for tau such that
 *  ||p + tau * u|| = delta
 *
 * We look for the null space vector u of B + lambda I, so that
 *   R * (p + tau * u) is minimized
 * and
 *   ||p + tau * u|| = delta
 * with R = (B + lambda I)
 * u is constructed so that ||R u || is as small as possible (actually u is
 * an approximation of an eigen vector).
 * R is an upper triangular matrix
 */
// -------------------------------------------------------------------------
real_type TRSOL::getZhatAndTau(const MATRIXVAR &R, const VECTORVAR &p,
            real_type delta,
            /* OUTPUTS */  VECTORVAR &zhat)
{
    VECTORVAR v = VECTORVAR::Zero(ndim);
    VECTORVAR z = VECTORVAR::Zero(ndim);
    VECTORVAR subR = VECTORVAR::Zero(ndim);

    real_type val = fabs(R(0,0));
    if ( val < epsil) {
        zhat(0) = 1;
        return 0.;
    }

#ifndef USE_CLINE_CONN_VANLOAN
    Eigen::HouseholderQR<MATRIXVAR> qr(R.transpose());
    MATRIXVAR Q = qr.householderQ();//.transpose();

    // take the vector with the smallest error to null space
    int jdx = 0;
    val = 1e20;
    for (uint i=0; i<ndim; ++i) {
        double cval = (R * Q.col(i)).squaredNorm();
        if (cval < val) {
            jdx = i;
            val = cval;
        }
    }
    zhat = Q.col(jdx);

#else // USE_CLINE_CONN_VANLOAN
    double w, wm;

    // Cline, Moler, Stewart and Wilkinson algorithm (condition estimator)
    // to solve a triangular system A y = d
    // with d selected so that y is large in norm
    for(int k=0; k<ndim; ++k) {
        val = fabs(val) * sign(-z(k));
        if (fabs(val - z(k)) > fabs(R(k,k))) {
            real_type scal = min(0.01, fabs(R(k,k)) / fabs(val - z(k)));
            val *= scal;
            z *= scal;
        }

        if (fabs(R(k, k)) < epsil) {
            w = 1;
            wm = 1;
        }
        else {
            w = (val - 1) / R(k, k);
            wm =-(val - 1) / R(k, k);
        }

        double s = abs(val - z(k));
        double sm = abs(val + z(k));
        for (int j=k; j<ndim; ++j) {
            sm += wm + fabs(z(j) + wm * R(k, j));
        }
        if (k < ndim-1) {
            z(k+1) = w * R(k, k+1) + z(k + 1);
            s += z.array().sum();
        }
        if (s < sm) {
            if (k < ndim-1) {
                z(k+1) = z(k+1) + (wm - w) * R(k, k+1);
            }
            w = wm;
        }
        z(k) = w;
    }

    // solve R * v = w, and ||z||=1
    solveLinearSystem(R, z, v);
    zhat = v / v.norm();
#endif

    // solution to the second order polynomial in tau (whatever zhat indeed).
    // among the two possibles solutions, the one yielding to the smaller amplitude
    // of ||p + tau z|| is selected
    real_type pTzhat = p.dot(zhat);
    real_type denom = pTzhat + sign(pTzhat) * sqrt(pTzhat * pTzhat + (delta * delta - p.squaredNorm())); // always > 0
    real_type tau = (delta * delta - p.squaredNorm()) / denom;
    if (debug) {
        printf("\t||R zhat||: %g,  tau=%g\n", (R * zhat).norm(), tau);
        std::cout << "\tzhat=" << zhat.transpose() << std::endl;
    }

    return tau;
}

// -------------------------------------------------------------------------
/** Estimate the singular Lagrange value of an upper triangular matrix R.
 * This is only one step of an iterative process.
 * Iteratively, this singular value, l*, tends toward to the smallest eigen
 * value, ls <= 0, and l* < -ls.
 *
 * Returns: ls is the singular value
 */
// -------------------------------------------------------------------------
real_type TRSOL::estimateSingularValue(const MATRIXVAR &B, const MATRIXVAR &R, real_type lambda, real_type lmbSingular, int index) {
    int rank = index-1;
    if (rank < 1) {
        return lmbSingular;
    }

#ifdef USE_EIGEN_VALUE_WHEN_NPD
    Eigen::VectorXcd eivals = B.eigenvalues();
    if(debug) {
        std::cout << "The eigenvalues of the 3x3 matrix of ones are:" << eivals.transpose() << std::endl;
        std::cout << "R=" << R << std::endl;
    }
    real_type lambdaS = lambda - eivals.real().minCoeff() * (1 + 100 * options.tolerance);

#else
    MATRIXVAR mB = B.topLeftCorner(rank+1, rank+1);
    MATRIXVAR mR = R.topLeftCorner(rank+1, rank+1);
    for(uint irank=rank; irank<ndim; ++irank) {
        mR.col(irank) = mB.col(irank);
    }
    real_type dirac =-R(rank, rank);
    VECTORVAR x = VECTORVAR::Zero(rank+1);
    x(rank) = dirac;
    VECTORVAR u = VECTORVAR::Zero(rank+1);
    solveLinearSystem(mR, x, u);

    #ifdef DEBUG
    MATRIXVAR Ei = MATRIXVAR::Zero(rank+1, rank+1);
    Ei(rank, rank) = 1;
    std::cout << "Ei=" << Ei << std::endl;
    std::cout << "B=" << B << std::endl;
    std::cout << "colB=" << colBi.transpose() << std::endl;
    std::cout << "x=" << x.transpose() << std::endl;
    std::cout << "u=" << u.transpose() << std::endl;
    std::cout << "check: " << ((B + dirac * Ei) * u).transpose() << std::endl;
    #endif
    if (debug) {
        std::cout << "u=" << u.transpose() << std::endl;
        //std::cout << R << std::endl;
        //std::cout << "\tsingular value: x=" << x.transpose() << std::endl;
        printf("\tSingular value: rank=%d, dirac=%g, ||u||=%g\n", rank, dirac, u.norm());
    }

    real_type lambdaS = lambda + dirac/(u.squaredNorm());
#endif

    if (debug) {
        printf("\tSingular Lambda Value: lambdaS=%g  lambda=%g lambdaS*=%g\n", lmbSingular, lambda, lambdaS);
    }

    // update lower bound
    return max(lambda, max(lmbSingular, lambdaS));
}

// -------------------------------------------------------------------------
