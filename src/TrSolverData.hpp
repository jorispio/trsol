/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef TRSOL_DATA_HPP
#define  TRSOL_DATA_HPP
// -------------------------------------------------------------------------

#include <float.h>
#include <type_traits>

#define epsil	DBL_EPSILON
//#define epsil std::numeric_limits<real_type>::epsilon()

#include <Eigen/Core>
#ifdef EIGEN2
#include <Eigen/Array>      // for cwise() and comparisons
USING_PART_OF_NAMESPACE_EIGEN
#endif

typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VECTORVAR; 
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MATRIXVAR;

typedef double real_type;
typedef unsigned int uint;

// -------------------------------------------------------------------------
#endif
