/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef TRSOLVER_CHOLESKY_HPP
#define TRSOLVER_CHOLESKY_HPP

#include "TrSolverData.hpp"

enum CholeskyTriangle {
	Upper,
	Lower
};

// -------------------------------------------------------------------------
class CholeskyDecomposition {	
	MATRIXVAR U;
	MATRIXVAR L;
	
	static
	int matrixCholeskyUpper(const MATRIXVAR &a, int n, MATRIXVAR &U);
	static
	int matrixCholeskyLower(const MATRIXVAR &a, int n, MATRIXVAR &L);
public:
	CholeskyDecomposition(MATRIXVAR &a, CholeskyTriangle triangle);
	
	//bool isPositiveDefinite();
	static
	int matrixcholesky(const MATRIXVAR &a, int n, CholeskyTriangle triangle, MATRIXVAR &b);	
};

// -------------------------------------------------------------------------
#endif
