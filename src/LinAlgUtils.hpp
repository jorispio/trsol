/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#include <algorithm>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/LU>			// for matrix inverse only.

#include "TrSolverData.hpp"
#include "TrSolverException.hpp"
#include "TrSolverCholeskyDecomposition.hpp"

// -------------------------------------------------------------------------
/** L1-norm of a matrix
 *
 */
// -------------------------------------------------------------------------
inline
real_type normMatrix(const MATRIXVAR &V, int n, int m)
{
//	real_type r;
//	int i, j;
//
//	VECTORVAR v = VECTORVAR::Zero(m);
//
//	for(j=0; j<m; j++)
//		for(i=0; i<n; i++)
//			v(j) += fabs(V(i,j));
//
//	r = v.maxCoeff();
//	return r;

#ifdef EIGEN2
	return  V.cwise().abs().colwise().sum().maxCoeff(); 
#else
    return  V.array().abs().colwise().sum().maxCoeff(); 
#endif
}
// -------------------------------------------------------------------------
/** Assess whether a matrix is symmetric.
 *
 */
// -------------------------------------------------------------------------
inline
bool isSymmetric(const MATRIXVAR &M) {
	return (M - M.transpose()).array().abs().maxCoeff() < 100. * epsil;
}
// -------------------------------------------------------------------------
/** Solve the linear system A x = b
 *
 * A and B must be matrices that have the same number of rows.
 *
 * If A is a square matrix, the system is solved as inv(A)*B.
 * If A is an n-by-n matrix and B is a column vector with n elements, then X = A\B
 * is the solution to the equation AX = B computed by Gaussian elimination with partial pivoting.
 *
 * Otherwise, when B is a column vector, the solution to the linear system 
 * is computed in the least squares sense (the solution minimizes norm(A*X - B)).
 * 
 */
// -------------------------------------------------------------------------
bool solveLinearSystem(const MATRIXVAR &A, const VECTORVAR &b, VECTORVAR &s)
{
	if (A.rows() != b.rows()) {
		throw TrSolverException("Unknown left division for provided arguments");
	}
	
	if (A.rows() != A.cols()) {
		s = (A.inverse()) * b;    
	} else {
		// least square solution
		s = A.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
	}
	return true;
}
// -------------------------------------------------------------------------
/** Solve the linear system A x = b
 * where x is a triangular matrix.
 * The system is solved with backward substitution.
 * triangularType = {Eigen::Upper, Eigen::Lower}
 *    Eigen::Upper if A is upper triangular
 *    Eigen::Lower if A is lower triangular
 */
// -------------------------------------------------------------------------
inline
void solveTriangularSystem(const MATRIXVAR &mA, CholeskyTriangle triangularType, const VECTORVAR &b, VECTORVAR &x) 
{
	if (triangularType == CholeskyTriangle::Upper) {
		x = mA.triangularView<Eigen::Upper>().solve<Eigen::OnTheLeft>(b);
	}
	else {
		x = mA.triangularView<Eigen::Lower>().solve<Eigen::OnTheLeft>(b);
	}
}
