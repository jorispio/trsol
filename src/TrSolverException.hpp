/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// -------------------------------------------------------------------------
#ifndef TR_SOLVER_EXCEPTION_HPP
#define TR_SOLVER_EXCEPTION_HPP
// -------------------------------------------------------------------------
#include <iostream>
#include <exception>

struct TrSolverException : public std::exception {
	char msg[1024];
	
    TrSolverException() {
    }
    
	TrSolverException(const char* str) {
		strcpy(msg, str);
	}
	
	const char * what () const throw () {
		return msg;
	}
};
// -------------------------------------------------------------------------
#endif