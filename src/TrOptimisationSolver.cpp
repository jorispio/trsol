/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <iostream>
#include <math.h>
#include <unistd.h>

#include "TrSolver.hpp"
#include "TrOptimisationSolver.hpp"

#define eps	DBL_EPSILON
// std::numeric_limits<real_type>::epsilon()

// -------------------------------------------------------------------------
TrOptimisationSolver::TrOptimisationSolver(TrSubProblem *problem) : problem(problem),
	options (TrOptimizerOptions())
{
    
}
// -------------------------------------------------------------------------
TrOptimisationSolver::TrOptimisationSolver(TrSubProblem *problem, const TrOptimizerOptions &options) : problem(problem), options(options){

}
// -------------------------------------------------------------------------
TrOptimisationSolver::~TrOptimisationSolver() {
	
}
// -------------------------------------------------------------------------
void TrOptimisationSolver::setOptions(const TrOptimizerOptions &optionsIn) {
	options = optionsIn;
}
// -------------------------------------------------------------------------
double TrOptimisationSolver::heuristicTrustRegionSize(double rho, double delta, const VECTORVAR &p) {
    if (rho < options.eta2) {
        delta = delta * options.nu1;
    } else {
        if ((rho > options.eta3) && (fabs(p.norm() - delta) < eps)) {
            delta = std::min(options.nu2 * delta, options.maxRadius);
        }
    }
    return delta;
}
// -------------------------------------------------------------------------
bool TrOptimisationSolver::solve(VECTORVAR &x) {
	int ndim = x.rows();
	if (problem->getProblemSize() != ndim) {
		printf("Error in problem dimension. Expected an initial guess of size %d, but it is of size %d\n", 
					problem->getProblemSize(), ndim);
		return false;
	}
	
	double dk = options.startRadius;
    real_type lambda = 0;
    VECTORVAR p = VECTORVAR::Zero(x.rows());
        
    double f2 = problem->fun(x);
    double m2 = f2;

	int iter = 0;
	if (options.verboseLevel > 1) {
		printIterationHeader(f2, x, dk);
	}
	
	++iter;
	bool done = false;
    while((iter <= options.maxIterations) && (!done)) {
        lambda = problem->solve(x, dk, p);
        VECTORVAR xNew = x + p;

		double f1 = problem->fun(xNew);
		MATRIXVAR Gk = problem->getPositiveHessian();
        double m1 = f1 + problem->getCurrentGradient().transpose() * p + 0.5 * p.transpose() * Gk * p;
		double rho = 1;
		if (fabs(m1 - m2) > 0) {
			rho = (f1 - f2) / (m1 - m2);
        }
			
		if (options.verboseLevel > 1) {
			printIteration(iter, f1, xNew, p, dk, rho, lambda);
		}
		
        dk = heuristicTrustRegionSize(rho, dk, p);

        f2 = f1;
        m2 = m1;
		if (rho > options.eta1) { // sufficient improvement
			x = xNew;
		}

		// stopping criterion
        if (p.norm() < options.errTol) {
            done = true;
        }
        ++iter;		
    }
	
	if (options.verboseLevel > 0) {
		printf("Summary:\n");
		printf("  Exitcode:          %d\n", problem->getSolver()->getInfo().exitflag);
		printf("  Iterations:        %d\n", iter);
		printf("  Lagrange variable: %12.8f\n", lambda);
		printf("  Solution         :"); std::cout << x.transpose() << std::endl;
	}
	
	return done;
}
// -------------------------------------------------------------------------
void TrOptimisationSolver::printIterationHeader(double f, const VECTORVAR &x, double dk) 
{
	if (problem->getProblemSize() == 1) {
		printf("Iter.        f(x2)          x1          |p|           d           rho       lambda       |g|\n");
		printf(" %3d  %12.6f  %12.6f               %12.6f\n", 0, f, x(0), dk);
	} else if (problem->getProblemSize() == 2) {
		printf("Iter.    f(x1, x2)          x1          x2          |p|           d           rho       lambda       |g|\n");
		printf(" %3d  %12.6f  %12.6f %12.6f              %12.6f\n", 0, f, x(0), x(1), dk);
	} else {
		printf("Iter.       f(x)          |p|           d           rho       lambda       |g|\n");
		printf(" %3d  %12.6f      %12.6f\n", 0, f, dk);			
	}
}
// -------------------------------------------------------------------------
void TrOptimisationSolver::printIteration(int iter, double fval, const VECTORVAR &x, const VECTORVAR &p, double dk, double rho, double lambda) 
{
	if (problem->getProblemSize() == 1) {
		printf(" %3d  %12.6f  %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f\n", iter, fval, x(0), p.norm(), dk, rho, lambda, problem->getCurrentGradient().norm());			
	} else if (problem->getProblemSize() == 2) {
		printf(" %3d  %12.6f  %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f\n", iter, fval, x(0), x(1), p.norm(), dk, rho, lambda, problem->getCurrentGradient().norm());
	} else {
		printf(" %3d  %12.6f  %12.6f %12.6f %12.6f %12.6f %12.6f\n", iter, fval, p.norm(), dk, rho, lambda, problem->getCurrentGradient().norm());
	}			
}	
// -------------------------------------------------------------------------
