/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "TrSolverCholeskyDecomposition.hpp"
#include "TrSolverData.hpp"
#include "TrSolverException.hpp"

CholeskyDecomposition::CholeskyDecomposition(MATRIXVAR &a, CholeskyTriangle triangle) 
{ 
    int n = a.cols();
    
    if(triangle == CholeskyTriangle::Upper) {    
        matrixcholesky(a, n,  triangle, U);	
    } else {
        matrixcholesky(a, n,  triangle, L);	
    }
}
    
// -------------------------------------------------------------------------
/** Cholesky decomposition
 *
 * The algorithm computes Cholesky decomposition of a symmetric
 * positive-definite matrix.
 * The result of an algorithm is a representation of matrix A as 
 * A = U'*U or A = L*L'.
 *
 * Inputs:
 *    A             symmetric positive definite matrix
 *    N             size of matrix A.
 *    triangle      to compute the {CholeskyTriangle::Upper, CholeskyTriangle::Lower}  triangular part
 *
 * Outputs:
 *    R       the result of factorization. 
 * If triangle=CholeskyTriangle::Upper, then R contains the upper triangular matrix U, A = U'*U.
 * If triangle=CholeskyTriangle::Lower, then R contains the lower triangular matrix L, A = L'*L.
 *
 */
int CholeskyDecomposition::matrixcholesky(const MATRIXVAR &A, int n, CholeskyTriangle triangle, MATRIXVAR &mR)
{
    if( n<=0 ) {
		throw TrSolverException("Wrong dimension for Cholesky");
    }
    if(( A.cols() != A.rows()) || (A.cols() != n)) {
		throw TrSolverException("Wrong dimension for Cholesky");
    }

	if(triangle == CholeskyTriangle::Upper) {
		return matrixCholeskyUpper(A, n, mR);
	}
    else {
		return matrixCholeskyLower(A, n, mR);
	}
}

// -------------------------------------------------------------------------
// Compute the Cholesky factorization A = U'*U.
int CholeskyDecomposition::matrixCholeskyUpper(const MATRIXVAR &A, int n, MATRIXVAR &mU)
{
	mU = MATRIXVAR::Zero(n, n);
	for(int j = 0; j < n; j++) {
        // Compute U(J,J) and test for non-positive-definiteness.        
		real_type ajj = A(j,j) - mU.col(j).head(j).squaredNorm();
        if( ajj <= 0 ) {
			mU(j,j) = ajj;
			return j + 1; // return order information
        }
		ajj = sqrt(ajj);		
        mU(j,j) = ajj;

        // Compute elements J+1:N of row J.
        if( j < n - 1 ) {
			real_type s = 1. / ajj;
			for(int icol = j + 1; icol <= n - 1; icol++) {
				real_type v = mU.col(icol).head(j).dot(mU.col(j).head(j));
                mU(j,icol) = A(j,icol) - v;
				mU(j,icol) *= s;
			}
            
            for(int icol = j + 1; icol <= n - 1; icol++) {
				//mU(j,icol) *= s;
			}
		}
	}

    return 0;
}

// -------------------------------------------------------------------------
// Compute the Cholesky factorization A = L*L'.
int CholeskyDecomposition::matrixCholeskyLower(const MATRIXVAR &A, int n, MATRIXVAR &mL)
{
    for(int j = 0; j <= n - 1; j++) {
        // Compute L(J,J) and test for non-positive-definiteness.		
		real_type ajj = A(j, j) - mL.row(j).head(j).squaredNorm();
        if( ajj <= 0 ) {
			mL(j, j) = ajj;
			return j + 1; // return order information
		}
        ajj = sqrt(ajj);
        mL(j, j) = ajj;

		// Compute elements J+1:N of column J.
        if( j<n-1 ) {
			real_type s = 1. / ajj;
			for(int irow = j + 1; irow <= n - 1; irow++) {
				real_type v = mL.row(irow).head(j).dot(mL.row(j).head(j));
                mL(irow, j) = A(irow, j) - v;
				mL(irow, j) *= s;
			}
		}
	}
    return 0;
}
// -------------------------------------------------------------------------