/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef TR_OPTIMIZATION_SOLVER_HPP
#define TR_OPTIMIZATION_SOLVER_HPP

#include "TrSolverOptions.hpp"
#include "TrSubProblem.hpp"

// -------------------------------------------------------------------------
class TrOptimisationSolver 
{
private:
	TrSubProblem *problem;
	TrOptimizerOptions options;
	
	double heuristicTrustRegionSize(double rho, double d, const VECTORVAR &p);

	void printIterationHeader(double f, const VECTORVAR &x, double dk);
	void printIteration(int iter, double f, const VECTORVAR &x, const VECTORVAR &p, double dk, double rho, double lambda);
public:
	TrOptimisationSolver(TrSubProblem *problem);
	TrOptimisationSolver(TrSubProblem *problem, const TrOptimizerOptions &options);
	~TrOptimisationSolver();
	
	void setOptions(const TrOptimizerOptions &options);
	bool solve(VECTORVAR &x);
};
// -------------------------------------------------------------------------
#endif