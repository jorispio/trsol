/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#ifndef TRSOLVER_HPP
#define TRSOLVER_HPP

#include <Eigen/Core>
#include <Eigen/LU>			// for matrix inverse only.
#ifdef EIGEN2
#include <Eigen/Array>      // for cwise() and comparisons
USING_PART_OF_NAMESPACE_EIGEN
#endif

#include "TrSolverData.hpp"
#include "TrSolverOptions.hpp"

/* ------------------------------------------------------------------------------------- */
enum TrSolverExitflag {
	ok=0,							// The function value f(x) has the relative accuracy specified by rtol
	ok_hardcase=1,
	ok_abs_accuracy=2,				// The function value f(x) has the absolute accuracy specified by atol
	too_many_iterations=-1,			// Failure to converge after itmax iterations.
	no_sufficient_improvment=-2,
	rounding_error=-3,				// Rounding errors prevent further progress.
	invalid_dimensions=-96,
	hessian_is_not_square=-97,
	hessian_is_not_symmetric=-98,
	invalid_input=-99
};
/* ------------------------------------------------------------------------------------- */
struct TrSolverInfo {
	int n;
	int iterations;
	double lambda;	
	VECTORVAR gradient;
	MATRIXVAR hessian;
	VECTORVAR p;
	TrSolverExitflag exitflag;
};

/* ------------------------------------------------------------------------------------- */
struct TrSolverStatistics {
	int nbCholesky;	
	double cpuTime;
};

/* ------------------------------------------------------------------------------------- */
class TRSOL
{
private:
    uint ndim;
    bool positive_definite;	
	int iterations;
	TrSolverOptions options;
	TrSolverInfo info;
	TrSolverStatistics statistics;
	bool debug;

	VECTORVAR p;

	bool checkOptions();
	bool checkInputs(const MATRIXVAR &Gk, const VECTORVAR &gk);
	void resetStatistics();
	
	real_type initialiseLagrange(const MATRIXVAR &Gk, const VECTORVAR &gk, real_type delta, real_type &lmbLower, real_type &lmbUpper, real_type &lmbSingular);
	real_type safeguardLagrange(real_type lmbs, real_type lmbl, real_type lmbu, real_type lambda);
	real_type estimateSingularValue(const MATRIXVAR &B, const MATRIXVAR &R, real_type lambda, real_type lmbSingular, int index);
		
	bool SolveSimple(const MATRIXVAR &R, const VECTORVAR &gk, double dk, VECTORVAR &pSol, real_type &lambda);
	
    real_type updateLagrangeMultiplier(MATRIXVAR &R, VECTORVAR &p, real_type dk, real_type lambda);

    real_type getZhatAndTau(const MATRIXVAR &R, const VECTORVAR &p, real_type dk,
            /* OUTPUTS */VECTORVAR &zhat);

    real_type pSolve(const VECTORVAR &gk, const MATRIXVAR &Gk, real_type dk, int n, TrSolverOptions options,
        VECTORVAR &rho);
public:
    TRSOL(uint n, bool posdef=true, bool debug=false);
    ~TRSOL() {};

	/** Solve TR subproblem with "spherical" radius constraint */
    real_type Solve(const VECTORVAR &gk, const MATRIXVAR &Gk, real_type dk,
        const TrSolverOptions &options,
        VECTORVAR &rho);

	/** Solve TR subproblem with elliptic constraint */
    real_type Solve_Ellipt(const VECTORVAR &k, const MATRIXVAR &Gk, const VECTORVAR &dk,
        const TrSolverOptions &options,
        VECTORVAR &rho);

	TrSolverInfo getInfo() const { return info; }
	int getNumberOfIterations() { return iterations; }
	TrSolverStatistics getStatistics() const { return statistics; }
};
/* ------------------------------------------------------------------------------------- */
#endif
