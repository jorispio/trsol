/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// -------------------------------------------------------------------------
#ifndef TRSOL_OPTIONS
#define TRSOL_OPTIONS

struct TrSolverOptions {
	double stopTrustRegionRadius; // stop when current radius below stopTrustRegionRadius
	double tolerance;			  // relative accuracy
	double abstolerance;		  // absolute accuracy
	int maxIterations;            // maximum number of iterations
	int verboseLevel;             // 0 no printing, 1 summary and final solution, 2 iterations
	
	TrSolverOptions() {
		stopTrustRegionRadius = 0.;
		abstolerance = 1e-8;
		tolerance = 1e-8;
		maxIterations = 20;
		verboseLevel = -1;
	}
};


struct TrOptimizerOptions {
	int maxIterations;            // maximum number of iterations
	int verboseLevel;             // <=0: no printing, 1: summary and final solution, 2: iterations
	
	double eta1; // sufficient improvement to accept step
	double eta2; // eta2>eta1, threshold on improvement when we deem the model too bad and we need reducing TR radius
	double eta3; // eta3>eta2, threshold on improvement when we deem the model good and we can increase TR radius
	double nu1;  // <1, TR halving coefficient if no sufficient improvement (i.e. the quadratic model is not good enough)
	double nu2;  // >1, multiplicative TR coefficient, when the improvement is very good and we can have confidence in the quadratic model 
	double startRadius; // start TR radius
	double maxRadius; // max TR radius
	double errTol; // error tolerance on ||p||
	double stopTrustRegionRadius; // stop when current radius below stopTrustRegionRadius
		
	TrOptimizerOptions() {
		stopTrustRegionRadius = 0;
		maxIterations = 100;
		verboseLevel = -1;
		startRadius = 2;
		eta1 = 0.2;
		eta2 = 0.25;
		eta3 = 0.75;
		nu1 = 0.25;
		nu2 = 2.0;
		maxRadius = 5;
		errTol = 1e-6;
	}
};
// -------------------------------------------------------------------------
#endif
