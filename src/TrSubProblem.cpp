/*
 * Copyright (c) 2010-2018 Joris OLYMPIO
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
#include <iostream>
#include "TrSubProblem.hpp"


// -------------------------------------------------------------------------
TrSubProblem::TrSubProblem(int n) : ndim(n), lambda (0),
    options (TrSolverOptions()) {	
	gk = VECTORVAR(n);
    Gk = MATRIXVAR(n, n);
	tr = new TRSOL(ndim, true, false);	
}

// -------------------------------------------------------------------------
TrSubProblem::TrSubProblem(int n, const TrSolverOptions &options)
	 : ndim(n), options (options), lambda (0)
{
	gk = VECTORVAR(n);
    Gk = MATRIXVAR(n, n);
	tr = new TRSOL(ndim, true, false);	
}

// -------------------------------------------------------------------------
TrSubProblem::~TrSubProblem() {
	delete tr;
}
// -------------------------------------------------------------------------
void TrSubProblem::setOptions(const TrSolverOptions &optionsIn) {
	options = optionsIn;
}
// -------------------------------------------------------------------------
/**
 */
// -------------------------------------------------------------------------
double TrSubProblem::solve(const VECTORVAR &x, double dk, VECTORVAR &p) {
	if (x.rows() != ndim) {
		printf("Problem size mismatch! Expected n=%d, but size(x) = %d\n", ndim, (int)x.rows());
	}
	
	gradient(x, gk);
	hessian(x, Gk);

	lambda =  tr->Solve(gk, Gk, dk, options, p);
	return lambda;
}
// -------------------------------------------------------------------------
double TrSubProblem::getLagrangeValue() {
	return lambda;
}
// -------------------------------------------------------------------------
MATRIXVAR TrSubProblem::getPositiveHessian() {
	if (lambda > 0) {
		return Gk + lambda * MATRIXVAR::Identity(ndim, ndim);
	}
	return Gk;
}
// -------------------------------------------------------------------------
