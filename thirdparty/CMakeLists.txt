cmake_minimum_required(VERSION 3.3)
PROJECT(trsol)

include(ExternalProject)

# ====================================================================
set(EIGEN_PREFIX "Eigen")
set(EIGEN_TEMP "EigenDownload")
set(EIGEN_URL "http://bitbucket.org/eigen/eigen/get/3.3.7.tar.gz")
set(EIGEN_HG  "https://bitbucket.org/eigen/eigen/")
set(EIGEN_URL_MD5 "")

# ====================================================================
ExternalProject_Add(${EIGEN_PREFIX}
    PREFIX ${EIGEN_TEMP}
    SOURCE_DIR  ${EIGEN_PREFIX}    
    
    URL ${EIGEN_URL}
    #HG_REPOSITORY ${EIGEN_HG}  
    
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
	LOG_DOWNLOAD 1
	LOG_BUILD 1    
    TIMEOUT 90
)
