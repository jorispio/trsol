PROJECT(trsol)
cmake_minimum_required(VERSION 3.3)

ADD_SUBDIRECTORY(ex1)
ADD_SUBDIRECTORY(ex2_branin)
ADD_SUBDIRECTORY(ex3_rosenbrock)
ADD_SUBDIRECTORY(ex4_mishrabird)
ADD_SUBDIRECTORY(ex5_woods)