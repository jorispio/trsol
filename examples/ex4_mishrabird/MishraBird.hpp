#ifndef MISHRA_BIRD_HPP
#define MISHRA_BIRD_HPP

// -------------------------------------------------------------------------
class SubProblem : public TrSubProblem
{
public:
    SubProblem(TrSolverOptions options) : TrSubProblem(2, options) {}
    ~SubProblem() {}

    // global minimum of -106.7645367 at (-3.1302468, -1.5821422)
    double fun(const VECTORVAR &x) {
        double cosx = cos(x(0));
        double siny = sin(x(1));
        return siny * exp((1 - cosx)*(1 - cosx)) + cosx * exp((1 - siny)*(1 - siny)) + pow(x(0) - x(1), 2);
    }

    void gradient(const VECTORVAR &x, VECTORVAR &gk) {
        double cosx = cos(x(0));
        double sinx = sin(x(0));
        double siny = sin(x(1));
        double cosy = cos(x(1));
        double exp1 = exp((1 - cosx)*(1 - cosx));
        double exp2 = exp((1 - siny)*(1 - siny));
        gk(0) = 2 * siny * sinx * (1 - cosx) * exp1 - sinx * exp2 + 2 * (x(0) - x(1));
        gk(1) = cosy * exp1 - 2 * cosx * cosy * (1 - siny) * exp2 - 2 * (x(0) - x(1));
    }

    void hessian(const VECTORVAR &x, MATRIXVAR &Gk) {
        double cosx = cos(x(0));
        double sinx = sin(x(0));
        double siny = sin(x(1));
        double cosy = cos(x(1));
        double exp1 = exp((1 - cosx)*(1 - cosx));
        double exp2 = exp((1 - siny)*(1 - siny));
        Gk(0, 0) = siny * 2 * (cosx * (1 - cosx) * exp1 + sinx * sinx * exp1  + (-sinx) * (1-cosx) * 2 * (-sinx) * exp1)
                    - cosx * exp2 + 2;
        Gk(0, 1) = cosy * 2 * sinx * (1 - cosx) * exp1 - sinx * 2 * (-cosy) * (1 - siny) * exp2 - 2;
        Gk(1, 0) = Gk(0, 1);
        Gk(1, 1) = -siny * exp1 + 2 * cosx * siny * (1 - siny) * exp2
                                + 2 * cosx * cosy * (cosy + 2 * (1 - siny) * (1 - siny) * (cosy) * exp2) + 2;
    }
};
// -------------------------------------------------------------------------
#endif