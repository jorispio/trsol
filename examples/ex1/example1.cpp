#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

#include "src/TrSolver.hpp"
#include "src/TrSolverCholeskyDecomposition.hpp"

Eigen::IOFormat FullFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");


void testCholesky(MATRIXVAR &Gk, double lambda, int n) {
    MATRIXVAR GkI = Gk + lambda * MATRIXVAR::Identity(n,n);
    MATRIXVAR b = MATRIXVAR::Zero(n,n);
    bool isPositiveDefinite = CholeskyDecomposition::matrixcholesky(GkI, n, CholeskyTriangle::Upper, b);

    printf("isPositiveDefinite(Gk + %.2f I) = %s\n", lambda, (isPositiveDefinite)?"yes":"no");
    //if (!isPositiveDefinite) {
    //    printf("U(Gk) =\n");
    //    std::cout << b.format(FullFmt) << std::endl;
    //}
}

//
void genericCase(VECTORVAR &gk, MATRIXVAR &Gk, double dk, int n, bool print) {
    VECTORVAR p = VECTORVAR::Zero(n);

    TrSolverOptions options = TrSolverOptions();
    options.maxIterations = 100;
    options.tolerance = 1e-8;

    TRSOL tr(n);
    double lambda = tr.Solve(gk, Gk, dk, options, p);

    if (print) {
        printf("\n");
        std::cout << "Consider the quadric: g dx + 1/2 dx' H dx" << std::endl;
        std::cout << "And the constraint: |dx| < r with r=" << dk << std::endl;
        std::cout << "With:" << std::endl;
        std::cout << "\tg = " << gk.transpose().format(FullFmt) << std::endl;
        std::cout << "\tH = " << Gk.format(FullFmt) << std::endl;
        std::cout << "Then the minimum is obtained for:" << std::endl;
        printf("\tLagrange multiplier lambda = %g\n", lambda);
        std::cout << "\tdx = " << p.transpose().format(FullFmt) << std::endl;
        printf("\tsolved in %d iterations\n", tr.getNumberOfIterations());
        printf("------------------------------------------------------\n\n");
    }
}

int main(int argc, char **arg)
{
    // case 1
    // We have a good quadric, thus lambda should be 0.
    // TRSOL should thus simply finds the minimum.
    // we set dk=1 because the solution xs is |xs|<1.
    VECTORVAR gk = VECTORVAR::Zero(1);
    MATRIXVAR Gk = MATRIXVAR::Zero(1,1);
    gk(0) = 96;
    Gk(0,0) = 152;
    double dk = 1;
    genericCase(gk, Gk, dk, 1, true);

    // case 2 - easy
    gk = VECTORVAR::Zero(3);
    Gk = MATRIXVAR::Zero(3,3);
    gk << 5, 0, 4;
    Gk(0,0) = 1; Gk(0,1) = 0; Gk(0,2) = 4;
    Gk(1,0) = 0; Gk(1,1) = 2; Gk(1,2) = 0;
    Gk(2,0) = 4; Gk(2,1) = 0; Gk(2,2) = 3;
    dk = 1;
    testCholesky(Gk, 0., 3);
    testCholesky(Gk, 4., 3);
    genericCase(gk, Gk, dk, 3, true);

    // case 2 - hard
    gk = VECTORVAR::Zero(3);
    Gk = MATRIXVAR::Zero(3,3);
    gk << 0, 2, 0.0001;
    Gk(0,0) = 1; Gk(0,1) = 0; Gk(0,2) = 4;
    Gk(1,0) = 0; Gk(1,1) = 2; Gk(1,2) = 0;
    Gk(2,0) = 4; Gk(2,1) = 0; Gk(2,2) = 3;
    dk = 1;
    testCholesky(Gk, 0., 3);
    testCholesky(Gk, 2.124, 3);
    genericCase(gk, Gk, dk, 3, true);

    return 0;
}
