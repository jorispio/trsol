#ifndef WOODCOLVILLE_HPP
#define WOODCOLVILLE_HPP

// -------------------------------------------------------------------------
class SubProblem : public TrSubProblem
{
public:
    SubProblem(TrSolverOptions options) : TrSubProblem(4, options) {}
    ~SubProblem() {}

    // global minimum of 0 at (1,1)
    double fun(const VECTORVAR &x) {
        return pow(100 * (x(1) - x(0)*x(0)), 2)
                + pow(1 - x(0), 2)
                + 90 * pow(x(3) - x(2)*x(2), 2)
                + pow(1 - x(2), 2)
                + 10.1 * (pow(x(1) - 1, 2) + pow(x(3) - 1, 2))
                + 19.8 * (x(1) - 1) * (x(3) - 1);
    }

    void gradient(const VECTORVAR &x, VECTORVAR &gk) {
        gk(0) =-10000. * 4. * x(0) * (x(1) - x(0)*x(0)) - 2. * (1 - x(0));
        gk(1) = 10000. * 2. * (x(1) - x(0)*x(0)) + 10.1 * 2 * (x(1) - 1.) + 19.8 * (x(3) - 1.);
        gk(2) =-90. * 4. * x(2) * (x(3) - x(2)*x(2)) - 2. * (1. - x(2));
        gk(3) = 90. * 2. * (x(3) - x(2)*x(2)) + 10.1 * 2. * (x(3) - 1.) + 19.8 * (x(1) - 1.);
    }

    void hessian(const VECTORVAR &x, MATRIXVAR &Gk) {
        VECTORVAR g = VECTORVAR::Zero(4);
        VECTORVAR gh = VECTORVAR::Zero(4);
        gradient(x, g);
        double h = 1e-5;
        for (int i=0; i<4; ++i) {
            VECTORVAR xh = VECTORVAR::Zero(4);
            xh(i) = h;
            gradient(x + xh, gh);
            Gk.col(i) = (gh - g) / h;
        }
        Gk = (Gk + Gk.transpose()).eval() / 2.;
    }
};
// -------------------------------------------------------------------------
#endif