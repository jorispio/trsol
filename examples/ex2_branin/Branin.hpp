#ifndef BRANIN_HPP
#define BRANIN_HPP

// -------------------------------------------------------------------------
class SubProblem : public TrSubProblem
{
public:
    SubProblem(TrSolverOptions options) : TrSubProblem(2, options) {}
    ~SubProblem() {}

    // Branin function
    double fun(const VECTORVAR &x) {
        return pow((x(1) - 0.129 * x(0) * x(0) + 1.6 * x(0) - 6), 2) + 6.07 * cos(x(0)) + 10;
    }

    void gradient(const VECTORVAR &x, VECTORVAR &gk) {
        gk(0) = 2 * (-2 * 0.129 * x(0) + 1.6) * (x(1) - 0.129 * x(0) * x(0) + 1.6 * x(0) - 6) - 6.07 * sin(x(0));
        gk(1) = 2 * (x(1) - 0.129 * x(0) * x(0) + 1.6 * x(0) - 6);
    }

    void hessian(const VECTORVAR &x, MATRIXVAR &Gk) {
        Gk(0,0) = 2 * (-2 * 0.129) * (x(1) - 0.129 * x(0) * x(0) + 1.6 * x(0) - 6) - 6.07 * cos(x(0))
                + 2 * (-2 * 0.129 * x(0) + 1.6) * (-2 * 0.129 * x(0) + 1.6);
        Gk(0,1) = 2 * (-2 * 0.129 * x(0) + 1.6);
        Gk(1,0) = Gk(0,1);
        Gk(1,1) = 2;
    }
};
// -------------------------------------------------------------------------
#endif