PROJECT(trsol)
cmake_minimum_required(VERSION 3.3)

SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -msse -msse2")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
# ===============================================================
INCLUDE_DIRECTORIES("../../")
INCLUDE_DIRECTORIES("../../thirdparty/Eigen")

LINK_LIBRARIES(trsol)

# ===============================================================

ADD_EXECUTABLE(exBranin
     example2.cpp 
)

install(TARGETS exBranin
    RUNTIME DESTINATION examples
)