#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>

#include "src/TrOptimisationSolver.hpp"
#include "Rosenbrock.hpp"

// -------------------------------------------------------------------------
//
int main(int argc, char **arg)
{
    VECTORVAR x = VECTORVAR::Zero(2);
    x << 3, 2;

    unsigned int start_time = clock();

    TrSolverOptions subPbOpts = TrSolverOptions();
    subPbOpts.maxIterations = 100;
    subPbOpts.tolerance = 1e-10;
    SubProblem *problem = new SubProblem(subPbOpts);

    TrOptimisationSolver *solver = new TrOptimisationSolver(problem);
    TrOptimizerOptions optionsAlg = TrOptimizerOptions();
    optionsAlg.startRadius = 2;
    optionsAlg.maxRadius = 4;
    optionsAlg.maxIterations = 100;
    optionsAlg.verboseLevel = 2;
    solver->setOptions(optionsAlg);

    solver->solve(x);

    unsigned int stop_time = clock();
    double cputime = difftime(stop_time, start_time);

    printf("cputime = %f\n", cputime / double(CLOCKS_PER_SEC));

    delete problem;

    return 0;
}
