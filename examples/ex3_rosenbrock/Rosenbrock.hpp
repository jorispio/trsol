#ifndef BRANIN_HPP
#define BRANIN_HPP

// -------------------------------------------------------------------------
class SubProblem : public TrSubProblem
{
public:
    SubProblem(TrSolverOptions options) : TrSubProblem(2, options) {}
    ~SubProblem() {}

    // Rosenbrock function, n>=2
    // global minimum of 0 at (1,1)
    double fun(const VECTORVAR &x) {
        double obj = 0.0;
        double temp = x[1] - x[0]*x[0];
        obj += 100.0*temp*temp;
        temp = 1.0 - x[0];
        obj += temp*temp;
        return obj;
    }

    void gradient(const VECTORVAR &x, VECTORVAR &gk) {
        gk(0) = -400.0 * x(0) * (x(1)-x(0)*x(0)) - 2.0*(1.0-x(0));
        gk(1) = 200.0 * (x(1) - x(0) * x(0));
    }

    void hessian(const VECTORVAR &x, MATRIXVAR &Gk) {
        Gk(0, 0) = 1200.0 * x(0) * x(0) - 400.0 * x(1) + 2.0;
        Gk(1, 1) = 200.0;
        Gk(0, 1) = -400.0 * x(0);
        Gk(1, 0) = Gk(0, 1);
    }
};
// -------------------------------------------------------------------------
#endif